/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.uploaderdownloader.impl.downloader;

import com.google.common.base.Charsets;
import com.smartupds.uploaderdownloader.api.Downloader;
import com.smartupds.uploaderdownloader.common.Resources;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.query.Binding;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.GraphQuery;
import org.eclipse.rdf4j.query.GraphQueryResult;
import org.eclipse.rdf4j.query.MalformedQueryException;
import org.eclipse.rdf4j.query.QueryEvaluationException;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.Rio;

/**
 * DataDownloader class that implements Downloader interface.
 * Note: use limit on the queries.
 * @author mafragias
 */
public class DataDownloader implements Downloader{
    
    private SPARQLRepository repo;
    private File target = new File("./workspace/output/" + DateTimeFormatter.ofPattern("dd-MM-yyyy_hh-mm").format(LocalDateTime.now())); // default destination folder
    private String query = "";
    private String format = Resources.TSV;
    private boolean append;
    
    public DataDownloader(String endpoint,String query){
        repo = new SPARQLRepository(endpoint);
        if (new File(query).isFile())
            this.query = getQueryFromFile(query);
        else
            this.query = query;
    }
    
    public DataDownloader(String endpoint,String query,String username, String password){
        this(endpoint,query);
        repo.setUsernameAndPassword(username, password);
    }
    
    /**
     * Download method that checks if it construct or select query for download.
     */
    @Override
    public void download() {
        if (query.toLowerCase().contains(Resources.CONSTRUCT))
            downloadConstructQuery(target.getAbsolutePath(),this.append);
        else
            downloadSelectQuery(target.getAbsolutePath());
    }
    
    private void downloadConstructQuery(String path,boolean append) {
            OutputStreamWriter writer = null;
            if (!(path.toLowerCase().endsWith(Resources.TTL) || path.toLowerCase().endsWith(Resources.N3)))
                path = path + ".ttl";
            Logger.getLogger(DataDownloader.class.getName()).log(Level.INFO, "Endpoint : {0}", repo.toString());
            try {
                writer = new OutputStreamWriter(new FileOutputStream(path,append),"UTF-8");
            } catch (FileNotFoundException | UnsupportedEncodingException ex) {
                Logger.getLogger(DataDownloader.class.getName()).log(Level.SEVERE, null, ex);
            }
            Logger.getLogger(DataDownloader.class.getName()).log(Level.INFO,"Initializing repository");
            repo.initialize();
            Logger.getLogger(DataDownloader.class.getName()).log(Level.INFO,"Getting repository connection");
            RepositoryConnection conn = repo.getConnection();
            Logger.getLogger(DataDownloader.class.getName()).log(Level.INFO,"Preparing graph query : \n".concat(query));
            GraphQuery graph = conn.prepareGraphQuery(query);
            Logger.getLogger(DataDownloader.class.getName()).log(Level.INFO,"Evaluating graph query");
            GraphQueryResult result = graph.evaluate();
            Logger.getLogger(DataDownloader.class.getName()).log(Level.INFO,"Successful query evaluation");
            while(result.hasNext()){
                Statement stmt = result.next();

                    if(stmt.getObject() instanceof IRI){
                        try {
                            writer.write("<"+stmt.getSubject()+"> <"+stmt.getPredicate()+"> <" + stmt.getObject() +">. \n");
                        } catch (IOException ex) {
                            Logger.getLogger(DataDownloader.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }else{
                        try {
                            writer.write("<"+stmt.getSubject()+"> <"+stmt.getPredicate()+"> \"" + stmt.getObject().stringValue().replace("\n", " ").replace("\"", "\\\"").replace("'", " ") +"\". \n");
                        } catch (IOException ex) {
                            Logger.getLogger(DataDownloader.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }

            }   Logger.getLogger(DataDownloader.class.getName()).log(Level.INFO,"File Saved at: ".concat(path));
            try {
                writer.close();
            } catch (IOException ex) {
                Logger.getLogger(DataDownloader.class.getName()).log(Level.SEVERE, null, ex);
            }
            Logger.getLogger(DataDownloader.class.getName()).log(Level.INFO,"Shutting down repository");
            conn.close();
            repo.shutDown();
    }

    private void downloadSelectQuery(String path) {
        if (!(path.toLowerCase().endsWith(Resources.CSV) || path.toLowerCase().endsWith(Resources.TSV)))
            path = path + "." + this.getFormat();
        
        Logger.getLogger(DataDownloader.class.getName()).log(Level.INFO,"Endpoint : ".concat(repo.toString()));
        try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(path,append), "UTF-8")) {
            Logger.getLogger(DataDownloader.class.getName()).log(Level.INFO,"Initializing repository");
            repo.initialize();
            Logger.getLogger(DataDownloader.class.getName()).log(Level.INFO,"Getting repository connection");
            RepositoryConnection conn = repo.getConnection();
            Logger.getLogger(DataDownloader.class.getName()).log(Level.INFO,"Preparing select query : \n".concat(query));
            TupleQuery select = conn.prepareTupleQuery(QueryLanguage.SPARQL,query);
            ArrayList<String> columns = new ArrayList<>();
//            ArrayList<String> tmp_names = new ArrayList<String>();
            Matcher m = Pattern.compile("(?<=select|SELECT)(.*)(?=where|WHERE)").matcher(this.query);
            if (m.find()){
                String to_harvest = m.group(1);
                Matcher n = Pattern.compile("\\s\\?([a-zA-Z_]+)").matcher(to_harvest);
                while (n.find()){
                    columns.add(n.group(1));
                }
            }
//            String[] tmp_names = this.query.split(" ");
//            boolean start = false;
//            for(String name:tmp_names){
//                if(name.toLowerCase().trim().contains("where"))
//                    break;
//                if(start && !name.trim().equalsIgnoreCase("distinct"))
//                    columns.add(name.replace("?", ""));
//                if(name.toLowerCase().trim().contains("select"))
//                    start = true;  
//            }
            Logger.getLogger(DataDownloader.class.getName()).log(Level.INFO,"Evaluationg select query");
            TupleQueryResult result = select.evaluate();
            Logger.getLogger(DataDownloader.class.getName()).log(Level.INFO,"Successful query evaluation");
            int count = 0;
            while (result.hasNext()){
                BindingSet set = result.next();
                Iterator<Binding> it = set.iterator();
                if(count==0 && columns.size()<1){
                    set.getBindingNames().forEach(name->{
                        columns.add(name);
                    });
                }
                if (count==0){
                    for (String name:columns)
                        writer.append(name+"\t");
                    writer.append("\n");
                }
                columns.forEach(name->{
                    try {
                        if(set.getBinding(name)!=null){
                            writer.append(set.getBinding(name).getValue().stringValue().replace("\n", " ")+"\t");
                        } else
                            writer.append("-\t");
                    } catch (IOException ex) {
                        Logger.getLogger(DataDownloader.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                });
                writer.append("\n");
                count ++;
            }
            Logger.getLogger(DataDownloader.class.getName()).log(Level.INFO,"File Saved at: ".concat(path));
            Logger.getLogger(DataDownloader.class.getName()).log(Level.INFO,"Shutting down repository");
            writer.close();
            conn.close();
            repo.shutDown();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(DataDownloader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataDownloader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(DataDownloader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private String getQueryFromFile(String file) {
        String q = "";
        try {
            BufferedReader reader = new BufferedReader( new InputStreamReader(new FileInputStream(file), "UTF8"));
            String row = "";
            while((row = reader.readLine())!=null)
                q = q + row + "\n";
        } catch (IOException ex) {
            Logger.getLogger(DataDownloader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return q;
    }
    
// Setters & Getters
    public void setTarget(String path){
        target = new File(path);
    }
    public File getTarget(){
        return target;
    }
    
    public void setFormat(String format){
        if (Resources.TSV.equalsIgnoreCase(format))
            this.format = Resources.TSV;
        else if (Resources.CSV.equalsIgnoreCase(format))
            this.format = Resources.CSV;
    }
    public String getFormat(){
        return format;
    }
    
    public void setAppend(boolean append){
        this.append = append;
    }
    public boolean getAppend(){
        return this.append;
    }


}
