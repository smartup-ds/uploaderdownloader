/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.uploaderdownloader.impl.uploader;

import com.smartupds.uploaderdownloader.api.Uploader;
import com.smartupds.uploaderdownloader.common.Utils;
import com.smartupds.uploaderdownloader.impl.downloader.DataDownloader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.eclipse.rdf4j.query.Update;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;

/**
 * DataUploader class that implements Uploader interface.
 * Use Split API (https://bitbucket.org/smartup-ds/rdfsplit.git) to resize the files
 * before uploading them to the SPARQL repository.
 * @author mafragias
 */
public class DataUploader implements Uploader {
    
    private SPARQLRepository repo;
    private File source = new File("./workspace/input/"); // default input folder
    private String graph = "https://"+DateTimeFormatter.ofPattern("dd-MM-yyyy_hh-mm").format(LocalDateTime.now()) +"/graph";
    
    public DataUploader(String endpoint){
        repo = new SPARQLRepository(endpoint);
    }
    
    public DataUploader(String endpoint,String username, String password){
        this(endpoint);
        repo.setUsernameAndPassword(username, password);
    }
    
    /**
     * Upload method that checks if it directory or a single file and then uploads
     * these files to the SPARQL repository. 
     */
    @Override
    public void upload() {
        Logger.getLogger(DataDownloader.class.getName()).log(Level.INFO, "Endpoint : {0}", repo.toString());
        
        if(source.isDirectory()){
            ArrayList<String> fileList = Utils.listFilesForFolder(source);
            int counter = 0;
            for (String file: fileList){
                counter++;
                Logger.getLogger(DataUploader.class.getName()).log(Level.INFO, "\t{0}/{1}\tPROCESSING | {2}", new Object[]{counter, fileList.size(), file});
                if (!file.endsWith(".trig"))
                    uploadFile(file);
                else 
                    uploadTrigFile(file);
                Logger.getLogger(DataUploader.class.getName()).log(Level.INFO, "\t{0}/{1}\tUPLOADED | {2}", new Object[]{counter, fileList.size(), file});
            }
        } else {
            if (!source.getAbsolutePath().endsWith(".trig"))
                uploadFile(source.getAbsolutePath());
            else 
                uploadTrigFile(source.getAbsolutePath());
            Logger.getLogger(DataUploader.class.getName()).log(Level.INFO, "UPLOADED {0}", new Object[]{source.getAbsolutePath()});
        }
    }
    
    private void uploadFile(String file){
        try {
            Logger.getLogger(DataUploader.class.getName()).log(Level.INFO,"Initializing repository");
            repo.initialize();
            BufferedReader reader = new BufferedReader( new InputStreamReader(new FileInputStream(file), "UTF8"));
            String row = "";
            String ttl = "";
            while((row = reader.readLine())!=null){
                ttl = ttl + row + "\n";
            }
            reader.close();
            Logger.getLogger(DataUploader.class.getName()).log(Level.INFO, "Uploading to graph: {0}", graph);
            String insertQ = "INSERT {"
                            + " GRAPH <"+graph+"> {"
                            + "     "+ttl+"}} WHERE {}";
            Logger.getLogger(DataUploader.class.getName()).log(Level.INFO,"Getting repository connection");
            RepositoryConnection conn = repo.getConnection();
            Logger.getLogger(DataUploader.class.getName()).log(Level.INFO, "Preparing Update query");
            Update insertQuery = conn.prepareUpdate(insertQ);
            insertQuery.execute();
            Logger.getLogger(DataUploader.class.getName()).log(Level.INFO,"Successful query execution");
            Logger.getLogger(DataUploader.class.getName()).log(Level.INFO,"Shutting down repository");
            conn.close();
            repo.shutDown();
        } catch (UnsupportedEncodingException | FileNotFoundException ex) {
            Logger.getLogger(DataUploader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataUploader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void uploadTrigFile(String file) {
        try {
            Logger.getLogger(DataUploader.class.getName()).log(Level.INFO,"Initializing repository");
            repo.initialize();
            BufferedReader reader = new BufferedReader( new InputStreamReader(new FileInputStream(file), "UTF8"));
            String row = "";
            String temp_graph = "";
            while((row = reader.readLine())!=null){
                Matcher m = Pattern.compile("^<").matcher(row);
                if (m.find()){
                    if (!temp_graph.equals("")){
                        // Upload.
                        String insertQ = "INSERT { GRAPH " +temp_graph+"} WHERE {}";
//                        System.out.println(insertQ);
                        Logger.getLogger(DataUploader.class.getName()).log(Level.INFO,"Getting repository connection");
                        RepositoryConnection conn = repo.getConnection();
                        Logger.getLogger(DataUploader.class.getName()).log(Level.INFO, "Preparing Update query");
                        Update insertQuery = conn.prepareUpdate(insertQ);
                        insertQuery.execute();
                        Logger.getLogger(DataUploader.class.getName()).log(Level.INFO,"Successful query execution");
                        conn.close();
                    }
                    temp_graph = row + "\n";
                } else {
                    if (!temp_graph.equals("")){
                        temp_graph = temp_graph + row + "\n";
                    }
                }
            }
            // upload last graph
            String insertQ = "INSERT { GRAPH" +temp_graph+"} WHERE {}";
//            System.out.println(insertQ);
            Logger.getLogger(DataUploader.class.getName()).log(Level.INFO,"Getting repository connection");
            RepositoryConnection conn = repo.getConnection();
            Logger.getLogger(DataUploader.class.getName()).log(Level.INFO, "Preparing Update query");
            Update insertQuery = conn.prepareUpdate(insertQ);
            insertQuery.execute();
            Logger.getLogger(DataUploader.class.getName()).log(Level.INFO,"Successful query execution");
            conn.close();
            reader.close();
            Logger.getLogger(DataUploader.class.getName()).log(Level.INFO,"Shutting down repository");
            repo.shutDown();
        } catch (UnsupportedEncodingException | FileNotFoundException ex) {
            Logger.getLogger(DataUploader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataUploader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    // Setters & Getters
    public void setSource(String path){
        source = new File(path);
    }
    public File getSource(){
        return source;
    }

    public void setGraph(String graph) {
        this.graph = graph;
    }
    public String getGraph() {
        return graph;
    }

}
