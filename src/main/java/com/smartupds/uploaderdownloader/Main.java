/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.uploaderdownloader;

import com.smartupds.uploaderdownloader.common.Utils;
import com.smartupds.uploaderdownloader.impl.downloader.DataDownloader;
import com.smartupds.uploaderdownloader.impl.uploader.DataUploader;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * This API downloads and uploads data from a SPARQL Endpoint.
 * Currently doesn't work for rdf files ->  to be added
 * @author mafragias
 */
public class Main {
    
    private static CommandLineParser parser = new DefaultParser();
    private static Options options = new Options();
    
    public static void main(String[] args){
        // Creating a default workspace
        Utils.createWorkSpace();
        // Creating a commandline options
        createOptions();
        try {
            
//            args = new String[]{"-e","http://vocab.getty.edu/sparql",
////                                "-u", "admin",
////                                "-p", "pharosadmin",
//                                "-q",   "select * where {\n" +
//                                        "	?x ?y ?z\n" +
//                                        "\n" +
//                                        "}limit 10",
//                                "-download"};
            
//            args = new String[]{"-e","http://vocab.getty.edu/sparql",
//"-q",   "CONSTRUCT {\n" +
//"	?f <http://www.researchspace.org/ontology/displayLabel> ?l\n" +
//"}where {\n" +
//"  ?f skos:inScheme ulan: ;\n" +
//"     gvp:prefLabelGVP/xl:literalForm ?l;\n" +
//"     gvp:broaderPreferred ulan:500000002.\n" +
//"}",
//                                "-append",
//"-d","./workspace/output/places_labels.ttl",
//"-download"};
            
//            
//            args = new String[]{"-e","https://query.wikidata.org/sparql",
//                                "-q",   "PREFIX bd: <http://www.bigdata.com/rdf#>\n" +
//"PREFIX wikibase: <http://wikiba.se/ontology#>\n" +
//"PREFIX wdtn: <http://www.wikidata.org/prop/direct-normalized/>\n" +
//"PREFIX wdt: <http://www.wikidata.org/prop/direct/>\n" +
//"CONSTRUCT {\n" +
//"#   ?photographer rdfs:label ?photographerLabel.\n" +
//"#   ?photographer a <https://pharos.artresearch.net/resource/fc/photographer>.\n" +
//"  ?photographer <https://pharos.artresearch.net/resource/fr/Person_has_Date_of_Birth> ?birth.\n" +
//"  ?photographer <https://pharos.artresearch.net/resource/fr/Person_has_Date_of_Death> ?death.\n" +
//"#   ?photographer <https://pharos.artresearch.net/resource/fr/Person_has_Gender> ?gender.\n" +
//"#   ?gender rdfs:label ?genderLabel.\n" +
//"} WHERE {\n" +
//"#   {\n" +
//"     ?photographer wdt:P106/wdt:P279* wd:Q33231 .\n" +
//"#   }UNION{\n" +
//"#     ?photographer wdt:P31 wd:Q672070 . \n" +
//"#   }UNION{\n" +
//"#     ?photographer wdt:P31 wd:Q2085381 . \n" +
//"#   }\n" +
//"#   OPTIONAL {\n" +
//"    ?photographer wdt:P570 ?death.\n" +
//"#     BIND(xsd:date(?death) AS ?death_date)\n" +
//"#   }\n" +
//"#   OPTIONAL {\n" +
//"#    ?wikidata_uri wdt:P569 ?birth .\n" +
//"#      BIND(xsd:date(?birth) AS ?birth_date)\n" +
//"#   }\n" +
//"#     OPTIONAL {\n" +
//"#       ?photographer <http://www.wikidata.org/prop/direct/P21> ?gender\n" +
//"# \n" +
//"#     }\n" +
//"#   SERVICE wikibase:label { bd:serviceParam wikibase:language \"en\".}\n" +
//"}",
//                                "-append",
//                                "-d","./workspace/output/wikidata-photographer-has-death.ttl",
//                                "-download"};
//
//           args = new String[]{ "-e","http://localhost:8889/bigdata/sparql",
//                                "-d","C:\\Users\\mafragias\\Documents\\WORKSPACE\\GitHub\\indexing-api\\workspace\\Output\\Indexing\\artists\\construct",
//                                "-g","https://test",
//                                "-upload"};
           
//        args = new String[]{    "-e","https://artresearch.net/sparql",
//                                "-u", "admin",
//                                "-p", "pharosadmin",
//                                "-d","C:\\Users\\mafragias\\Documents\\PHAROS\\pharos-mappings\\constants\\categories-structured_search.trig",
//                                "-upload"};

//        args = new String[]{    "-e","https://collection.itatti.harvard.edu/sparql",
//                                "-u", "admin",
//                                "-p", "vitadmin",
//                                "-d","C:\\Users\\mafragias\\Documents\\WORKSPACE\\NetBeansProjects\\DataPreprocessing\\replaced\\GRAPH_PROVIDERS.trig",
//                                "-upload"};
//
//           
//           
////           args = new String[]{ "-e","https://dev.artresearch.net/sparql",
////                                "-u", "admin",
////                                "-p", "pharosadmin",
////                                "-q","PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
////"select ?uri ?label where {\n" +
////"  GRAPH <https://pharos.artresearch.net/resource/itatti/fcrs> {\n" +
////"    ?uri a <https://pharos.artresearch.net/resource/custom/fc/work>.\n" +
////"  }\n" +
////"    ?uri rdfs:label ?label\n" +
////"  \n" +
////"}",
////                                "-d","C:\\Users\\mafragias\\Documents\\Data_For_Instance_Matching\\Artworks\\to_hash\\artworks-itatti.tsv",
//////                                "-append",
////                                "-download"};
           args = new String[]{ "-e","https://artresearch.net/sparql",
                                "-u", "admin",
                                "-p", "pharosadmin",
                                "-q","PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
"PREFIX fr: <https://pharos.artresearch.net/resource/fr/>\n" +
"PREFIX fts: <http://www.bigdata.com/rdf/fts#>\n" +
"PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>\n" +
"PREFIX owl: <http://www.w3.org/2002/07/owl#>\n" +
"PREFIX fc: <https://pharos.artresearch.net/resource/fc/>\n" +
"PREFIX custom: <https://pharos.artresearch.net/resource/custom/fc/>\n"
                   + "CONSTRUCT {\n" +
"  ?type rdfs:label ?typeLabel.\n" +
"}\n" +
"where {\n" +
"  ?s a custom:institution.\n" +
"  ?s crm:P2_has_type ?type.\n" +
"  ?type rdfs:label ?typeLabel.\n" +
"  FILTER(datatype(?typeLabel)=xsd:string)\n" +
"}",
                                "-d","./workspace/type__labels.n3",
//                                "-append",
                                "-download"};
//
//           args = new String[]{ "-e","https://artresearch.net/sparql",
//                                "-u", "admin",
//                                "-p", "pharosadmin",
//                                "-q",   "PREFIX fr: <https://pharos.artresearch.net/resource/fr/>\n"
//                   + "PREFIX fc: <https://pharos.artresearch.net/resource/fc/>\n" +
//"CONSTRUCT {\n" +
//"  #GRAPH <https:/.artresearch.net/resource/materializations/aat/materials/graph> {\n" +
//"  	  ?subject fr:Work_consists_of_Material ?generic1.\n" +
//"      ?subject fr:Work_consists_of_Material ?generic2.\n" +
//"      ?subject fr:Work_consists_of_Material ?generic3.\n" +
//"      ?subject fr:Work_consists_of_Material ?generic4.\n" +
//"      ?subject fr:Work_consists_of_Material ?generic5.\n" +
//"      ?subject fr:Work_consists_of_Material ?generic6.\n" +
//"      ?subject fr:Work_consists_of_Material ?generic7.\n" +
//"      ?subject fr:Work_consists_of_Material ?generic8.\n" +
//"      ?subject fr:Work_consists_of_Material ?generic9.\n" +
//"      ?subject fr:Work_consists_of_Material ?generic10.\n" +
//"      ?subject fr:Work_consists_of_Material ?generic11.\n" +
//"      ?subject fr:Work_consists_of_Material ?generic12.\n" +
//"      ?subject fr:Work_consists_of_Material ?generic13.\n" +
//"      ?subject fr:Work_consists_of_Material ?generic14.\n" +
//"      ?generic1 a fc:material.\n" +
//"      ?generic2 a fc:material.\n" +
//"      ?generic3 a fc:material.\n" +
//"      ?generic4 a fc:material.\n" +
//"      ?generic5 a fc:material.\n" +
//"      ?generic6 a fc:material.\n" +
//"      ?generic7 a fc:material.\n" +
//"      ?generic8 a fc:material.\n" +
//"      ?generic9 a fc:material.\n" +
//"      ?generic10 a fc:material.\n" +
//"      ?generic11 a fc:material.\n" +
//"      ?generic12 a fc:material.\n" +
//"      ?generic13 a fc:material.\n" +
//"      ?generic14 a fc:material.\n" +
//"  #}\n" +
//"}\n" +
//"\n" +
//"#select *\n" +
//"WHERE {\n" +
//"  ?suggestion a fc:material.\n" +
//"  ?subject a fc:work;\n" +
//"      fr:Work_consists_of_Material ?suggestion.\n" +
//"  ?suggestion <http://vocab.getty.edu/ontology#broaderPreferred> ?generic1.\n" +
//"  ?generic1 <http://vocab.getty.edu/ontology#broaderPreferred> ?generic2.\n" +
//"  ?generic2 <http://vocab.getty.edu/ontology#broaderPreferred> ?generic3.\n" +
//"  ?generic3 <http://vocab.getty.edu/ontology#broaderPreferred> ?generic4.\n" +
//"  ?generic4 <http://vocab.getty.edu/ontology#broaderPreferred> ?generic5.\n" +
//"  ?generic5 <http://vocab.getty.edu/ontology#broaderPreferred> ?generic6.\n" +
//"  ?generic6 <http://vocab.getty.edu/ontology#broaderPreferred> ?generic7.\n" +
//"#  ?generic7 <http://vocab.getty.edu/ontology#broaderPreferred> ?generic8.\n" +
//"#  ?generic8 <http://vocab.getty.edu/ontology#broaderPreferred> ?generic9.\n" +
//"#  ?generic9 <http://vocab.getty.edu/ontology#broaderPreferred> ?generic10.\n" +
//"#  ?generic10 <http://vocab.getty.edu/ontology#broaderPreferred> ?generic11.\n" +
//"#  ?generic11 <http://vocab.getty.edu/ontology#broaderPreferred> ?generic12.\n" +
//"#  ?generic12 <http://vocab.getty.edu/ontology#broaderPreferred> ?generic13.\n" +
//"#  ?generic13 <http://vocab.getty.edu/ontology#broaderPreferred> ?generic14.\n" +
//"	?generic7 a <http://vocab.getty.edu/ontology#Facet>\n" +
//"}",
//                                "-d","./workspace/output/construct_materializations.ttl",
//                                "-append",
//                                "-download"};

//           args = new String[]{ "-e","http://213.171.209.34:10085/blazegraph/sparql",
//                                "-u", "admin",
//                                "-p", "pharosadmin",
//                                "-q","CONSTRUCT {\n" +
//"  ?city <http://www.geonames.org/ontology#name> ?y.\n" +
//"  #?city <http://www.geonames.org/ontology#population> ?population.\n" +
//"  ?city <http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat.\n" +
//"  ?city <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?lng.\n" +
//"  ?city <http://www.cidoc-crm.org/cidoc-crm/P2_has_type> <http://vocab.getty.edu/aat/300008389>.\n" +
//"  ?city <http://www.geonames.org/ontology#parentCountry> ?country.\n" +
//"} WHERE {\n" +
//"  SERVICE <https://artresearch.net/sparql> {\n" +
//"  	?city a <https://pharos.artresearch.net/resource/fc/place>.\n" +
//"  }\n" +
//"  ?city <http://www.geonames.org/ontology#name> ?y.\n" +
//"  #?city <http://www.geonames.org/ontology#population> ?population.\n" +
//"  ?city <http://www.w3.org/2003/01/geo/wgs84_pos#lat> ?lat.\n" +
//"  ?city <http://www.w3.org/2003/01/geo/wgs84_pos#long> ?lng.\n" +
//"  ?city <http://www.geonames.org/ontology#parentCountry> ?country.\n" +
//"}",
//                                "-d","./workspace/output/cities.ttl",
//                                "-append",
//                                "-download"};

            // Parsing command line options
            CommandLine line = parser.parse( options, args  );
            handleCommandLine(line);

        } catch( ParseException exp ) {
            printOptions();
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Reason : ".concat(exp.getMessage()));
        }
    }

    private static void handleCommandLine(CommandLine line) {
        if (line.hasOption("download") && line.hasOption("q")){
            Logger.getLogger(Main.class.getName()).log(Level.INFO,"Downloading process started.");
            DataDownloader downloader;
            
            if (line.hasOption("u") && line.hasOption("p")){
                downloader = new DataDownloader(line.getOptionValue("e"),line.getOptionValue("q"),line.getOptionValue("u"),line.getOptionValue("p"));
            } else 
                downloader = new DataDownloader(line.getOptionValue("e"),line.getOptionValue("q"));
            
            if (line.hasOption("d"))
                downloader.setTarget(line.getOptionValue("d"));
            
            if (line.hasOption("append"))
                downloader.setAppend(true);
            else
                downloader.setAppend(false);
            
            if (line.hasOption("format"))
                downloader.setFormat(line.getOptionValue("format"));
            
            downloader.download();
            
            Logger.getLogger(Main.class.getName()).log(Level.INFO,"Downloading process completed.");
        } else if (line.hasOption("upload")){
            Logger.getLogger(Main.class.getName()).log(Level.INFO,"Uploading process started.");
            DataUploader uploader;
            if (line.hasOption("u") && line.hasOption("p")){
                uploader = new DataUploader(line.getOptionValue("e"),line.getOptionValue("u"),line.getOptionValue("p"));
            } else 
                uploader = new DataUploader(line.getOptionValue("e"));
            
            if (line.hasOption("d"))
                uploader.setSource(line.getOptionValue("d"));
            
            if (line.hasOption("g"))
                uploader.setGraph(line.getOptionValue("g"));
            
            uploader.upload();
            
            Logger.getLogger(Main.class.getName()).log(Level.INFO,"Uploading process completed.");
        } else {
            printOptions();
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Reason : ".concat("Not enough arguments."));
        }
    }
    
    private static void createOptions() {
        Option endpoint = new Option("e",true,"Endpoint to retrieve or ingest data : -e [sparql_endpoint]");
        endpoint.setArgName("endpoint");
        endpoint.setRequired(true);
        Option username = new Option("u",true,"SPARQL endpoint username : -u [username]");
        username.setArgName("username");
        Option password = new Option("p",true,"SPARQL endpoint password : -p [password]");
        password.setArgName("password");
        Option query = new Option("q",true,"Download Query : -q [query_file_or_text]");
        query.setArgName("query");
        Option graph = new Option("g",true,"Graph name to input the data : -g [graph_name]");
        graph.setArgName("graph");
        Option directory = new Option("d",true,"Directory/file as input for upload and as output for download : -d [directory]");
        directory.setArgName("directory");
        Option upload = new Option("upload",false,"Upload flag : -upload");
        Option download = new Option("download",false,"Download flag : -download");
        Option append = new Option("append",false,"Append results to a specific file : -append");

        options.addOption(endpoint);
        options.addOption(username);
        options.addOption(password);
        options.addOption(query);
        options.addOption(graph);
        options.addOption(directory);
        options.addOption(upload);
        options.addOption(download);
        options.addOption(append);
    }

    private static void printOptions() {
        String header = "\nChoose from options below:\n\n";
        String footer = "\nParsing failed.";
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("java -jar UploaderDownloader-0.1-assembly.jar", header, options, footer, true);
    }

}
