/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.uploaderdownloader.api;

/**
 * Interface used to create classes, that upload data to a SPARQL repository.
 * @author mafragias
 */
public interface Uploader {
    
    /**
     * This method is used to upload data to a specified graph in the SPARQL repository.
     */
    public void upload();
    
}
