/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.uploaderdownloader.api;

/**
 * Interface used to create classes, that download data from a SPARQL repository.
 * @author mafragias
 */
public interface Downloader {
    
    /**
     * This method is used to download data and save them in a specified path.
     */
    public void download();
    
}
