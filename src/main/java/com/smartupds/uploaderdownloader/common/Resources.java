/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.uploaderdownloader.common;

/**
 * This class contains universal variables.
 * @author mafragias
 */
public class Resources {

    public static final String WORKSPACE = "./workspace/";
    public static final String OUTPUT = WORKSPACE + "output/";
    public static final String INPUT = WORKSPACE + "input/";
    
    // Key words
    public static final String CONSTRUCT = "construct";
    
    // File extentions 
    public static final String CSV = "csv";
    public static final String TSV = "tsv";
    public static final String TTL = "ttl";
    public static final String N3 = "n3";
    
    public static final String IMAGE_IRI = "https://pharos.artresearch.net/custom/has_Image";
}
