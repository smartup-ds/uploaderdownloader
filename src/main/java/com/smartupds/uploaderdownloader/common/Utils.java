/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.uploaderdownloader.common;

import com.smartupds.uploaderdownloader.impl.uploader.DataUploader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.rdf4j.query.Update;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;

/**
 * This class contains useful generic methods.
 * @author mafragias
 */
public class Utils {

    /**
     * Method to create a default workspace.
     */
    public static void createWorkSpace() {
        File workspace = new File(Resources.WORKSPACE);
        File output = new File(Resources.OUTPUT);
        File input = new File(Resources.INPUT);
        if (!workspace.exists() || !output.exists() || !input.exists()){
            Logger.getLogger(Utils.class.getName()).log(Level.INFO,"Creating default workspace.");
            if (!workspace.exists())
                workspace.mkdir();
            if (!output.exists())
                output.mkdir();
            if (!input.exists())
                input.mkdir();
            Logger.getLogger(Utils.class.getName()).log(Level.INFO,"Default workspace created.");
        }
    }
    
    /**
     * Method to list files in a directory
     * @param folder
     * @return
     */
    public static ArrayList<String> listFilesForFolder(final File folder) {
        ArrayList<String> filePaths = new ArrayList<>(); 
        for (final File fileEntry : folder.listFiles()) {
            if ( fileEntry.getName().contains("desktop.ini")) {
                //dead
            } else {
                filePaths.add(folder.getAbsoluteFile() + "\\" + fileEntry.getName());
            }
        }
        return filePaths;
    }
    
}
