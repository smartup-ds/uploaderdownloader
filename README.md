# UD API #

This API uploads/downloads data to/from a SPARQL repository.

### Input Arguments

* **e [required]** (endpoint) : SPARQL endpoint to be used for uploading and downloading.
* **u [optional]** (username) : username for the SPARQL endpoint.
* **p [optional]** (password) : password for the SPARQL endpoint.
* **q [optional but required for downloading]** (query) : CONSTRUCT or SELECT query to be used for downloading data.
* **g [optional]** (graph) : name of the graph to save the upload data.
* **d [optional]** (directory) : path of the file or folder for the data for uploading and to download.
* **upload [optional but required for uploading]** : upload flag used for uploading data.
* **download [optional but required for downloading]** : download flag used for downloading data.

### Bash commands

UPLOAD :

```bash
$ java -jar target/UploaderDownloader-0.1-assembly.jar -upload -e [sparql_endpoint] -u [username] -p [password]  -d [source_directory] -g [graph_name]
```

DOWNLOAD :

```bash
$ java -jar target/UploaderDownloader-0.1-assembly.jar -download -e [sparql_endpoint] -u [username] -p [password] -q [sparql_query] -d [target_directory]
```

### Default workspace

* **./workspace/input/** : this is the default location of the input files to upload.
* **./workspace/output/** : this is the default destination of the data to be downloaded. 